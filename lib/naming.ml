type t = string list

let string_split_on f s =
  let acc_name_opt, acc_segments =
    String.fold_left
      (fun (acc_name_opt, acc_segments) c ->
        match acc_name_opt with
        | None ->
            let acc_name = Buffer.create 5 in
            Buffer.add_char acc_name c;
            (Some acc_name, acc_segments)
        | Some acc_name when f c ->
            let acc_name' = Buffer.create 5 in
            Buffer.add_char acc_name' c;
            (Some acc_name', acc_name :: acc_segments)
        | Some acc_name ->
            Buffer.add_char acc_name c;
            (acc_name_opt, acc_segments))
      (None, []) s
  in
  let segments =
    match acc_name_opt with
    | Some acc_name -> acc_name :: acc_segments
    | _ -> acc_segments
  in
  segments |> List.map Buffer.contents |> List.rev

let of_camlcase name =
  name
  |> string_split_on (function 'A' .. 'Z' -> true | _ -> false)
  |> List.map String.lowercase

let of_snakecase name = name |> String.split_on_char '_'
let to_snakecase v = v |> String.concat "_"
let to_title v = v |> String.concat " " |> String.capitalize

let rec strip_prefix ~prefix v =
  match (prefix, v) with
  | p :: prefix, v :: vs when String.equal p v -> strip_prefix ~prefix vs
  | _ -> v
