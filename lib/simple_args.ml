type 'a arg =
  | Pair : 'a arg * 'b arg -> ('a * 'b) arg
  | String_opt : string * string -> string option arg
  | Switch : string * string -> bool arg
  | Unit : unit arg

let arg_set_opt_string r : Arg.spec = Arg.String (fun s -> r := Some s)

type args_spec = (string * Arg.spec * string) list

let rec parse_arg : type a. a arg -> args_spec * (unit -> a) =
 fun arg ->
  match arg with
  | Pair (x, y) ->
      let as1, f1 = parse_arg x in
      let as2, f2 = parse_arg y in
      (as1 @ as2, fun () -> (f1 (), f2 ()))
  | String_opt (n, d) ->
      let r = ref None in
      ([ (n, arg_set_opt_string r, d) ], fun () -> !r)
  | Switch (n, d) ->
      let r = ref false in
      ([ (n, Arg.Set r, d) ], fun () -> !r)
  | Unit -> ([], Fun.id)

let ( <*> ) arg1 arg2 = Pair (arg1, arg2)

let parse_arg_run :
    type a.
    ?usage:(string -> string) -> a arg -> (a -> string list -> unit) -> unit =
 fun ?usage arg f ->
  let usage_msg =
    match usage with
    | Some usage -> usage Sys.argv.(0)
    | None ->
        Printf.sprintf "Usage: %s [options] [open_api_files ...]\nOptions are:"
          Sys.argv.(0)
  in
  let args_spec, getter = parse_arg arg in
  let anon = ref [] in
  Arg.parse args_spec (fun s -> anon := s :: !anon) usage_msg;
  f (getter ()) (List.rev !anon)
