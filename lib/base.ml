let sf = Format.asprintf

let with_open_in file read_f =
  let chan = open_in file in
  try
    let value = read_f chan in
    close_in chan;
    value
  with x ->
    close_in chan;
    raise x

let read_file filename =
  with_open_in filename @@ fun ch ->
  let buffer = Buffer.create 512 in
  let bytes = Bytes.create 512 in
  let rec loop () =
    let len = input ch bytes 0 512 in
    if len > 0 then (
      Buffer.add_subbytes buffer bytes 0 len;
      loop ())
  in
  loop ();
  Buffer.contents buffer

(* rex *)

let rex ?opts r = (r, Re.compile (Re.Perl.re ?opts r))
let show_rex = fst
let ( =~ ) s (_, r) = Re.execp r s
let ( =~! ) s (_, r) = not (Re.execp r s)

let get_group group index =
  match Re.Group.get group index with
  | exception Not_found ->
      invalid_arg
        "regular expression has not enough capture groups for its usage, did \
         you forget parentheses?"
  | value -> value

let ( =~* ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1)

let ( =~** ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1, get_group group 2)

let ( =~*** ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group -> Some (get_group group 1, get_group group 2, get_group group 3)

let ( =~**** ) s (_, r) =
  match Re.exec_opt r s with
  | None -> None
  | Some group ->
      Some
        ( get_group group 1,
          get_group group 2,
          get_group group 3,
          get_group group 4 )

let matches s (_, r) = Re.all r s |> List.map (fun g -> get_group g 1)

let replace_string ?pos ?len ?all (_, r) ~by s =
  Re.replace_string ?pos ?len ?all r ~by s
