let lines ~trailer n s =
  let lines = String.split_on_char '\n' s in
  let len = List.length lines in
  if len <= n then s
  else
    List.filteri (fun i _ -> i < n) lines @ [ trailer (len - n) ]
    |> String.concat "\n"

let middle ?(filler = " ... ") n s =
  let len = String.length s in
  if len > n then
    let len_remaining = n - String.length filler in
    let prefix_length = len_remaining / 2 in
    let prefix = String.sub s 0 prefix_length in
    let len_remaining = len_remaining - prefix_length in
    let suffix = String.sub s (len - len_remaining) len_remaining in
    prefix ^ filler ^ suffix
  else s

let right ?(filler = "...") n s =
  let len = String.length s in
  if len > n then
    let len_remaining = n - String.length filler in
    let prefix = String.sub s 0 len_remaining in
    prefix ^ filler
  else s

let to_first_line ?(trailer = " ...") s =
  match String.split_on_char '\n' s with
  | [ s ] -> s
  | s :: _ -> s ^ trailer
  | [] -> ""
