# Py2ml

Generates Tezt boilerplate from pytest files.

At the moment, it generates:

 - licence header
 - empty stubs from top-level definitions, keeping only doc comments
   and argument names
 - a call to `Protocol.register_test` per test class
 - a call to `Log.info` per test case
 - a registration function

It does not inline the original Python code in the tezt file. This
would've convenient, but unfortunately the `pyre-ast` library used to
parse and represent Python source code does not contain a pretty
printer.

## Installation

```
dune build
opam install .
```

## Usage

Call it like this:

```
dune exec bin/main.exe -- <input_pytest.py>
```

Example:


With input `test_p2p.py`

```python
import time
import pytest
from tools import constants
from launchers.sandbox import Sandbox


NUM_NODES = 5
NUM_RETRIES = 20  # empirical values for testing a liveness property
POLLING_TIME = 10  # NUM_RETRY * POLLING_TIME = 200s, should be conservative


@pytest.mark.multinode
@pytest.mark.incremental
class TestTrustedRing:
    """This test sets up a network of public peers (running the default
    p2p protocol), with no initial bootstrap peers. It initializes a
    trusted ring relationship, and checks that points are advertised
    correctly to the whole network."""

    def test_init(self, sandbox: Sandbox):
        for i in range(NUM_NODES):
            sandbox.add_node(
                i,
                private=False,
                peers=[],
                params=constants.NODE_PARAMS,
                config_client=False,
            )

    def test_no_peers(self, sandbox: Sandbox):
        """Initially, nobody knows other peers."""
        for client in sandbox.all_clients():
            res = client.p2p_stat()
            assert not res.peers

    def test_add_peers(self, sandbox: Sandbox):
        """Set up a trusted ring topology."""
        base_p2p = sandbox.p2p
        for i in range(NUM_NODES):
            client = sandbox.client(i)
            client.trust_peer(base_p2p + ((i + 1) % NUM_NODES))

# ...
```

outputs

```ocaml
(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* ...                                                                       *)
(*                                                                           *)
(*****************************************************************************)

(* Testing
   -------
   Component:    TODO
   Invocation:   dune exec tezt/tests/main.exe -- --file p2p.ml
   Subject:      TODO
*)

let num_nodes = assert false
let num_retries = assert false
let polling_time = assert false

(* This test sets up a network of public peers (running the default
   p2p protocol), with no initial bootstrap peers. It initializes a
   trusted ring relationship, and checks that points are advertised
   correctly to the whole network. *)
let test_trusted_ring =
  Protocol.register_test ~__FILE__ ~title:"Trusted ring"
    ~tags:[ "trusted"; "ring" ]
  @@ fun _protocol ->
  Log.info "Init";
  (* Initially, nobody knows other peers. *)
  Log.info "No peers";
  (* Set up a trusted ring topology. *)
  Log.info "Add peers";
  (* Everyone should be connected to everyone else. This is a
     liveness property. Its realization depends on the timing of the
     p2p maintenance process. The check is repeated up to NUM_RETRIES
     times with a POLLING_TIME seconds wait. *)
  Log.info "Check clique";
  (* Test various assumptions on the point/peer tables.
     Each peer has exactly one trusted neighbor. Tables don't
     contain their own peer/point id and contain exactly NUM_NODES - 1
     values.
     The previous test should guarantee that maintenance has been
     performed when this test is run. *)
  (* *<... snip ...> *)
  unit

let register ~protocols = test_trusted_ring protocols
```
