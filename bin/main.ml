open Py2ml
open Py2ml.Base

let strip_prefix ~prefix s =
  if String.starts_with ~prefix s then
    String.(sub s (length prefix) (length s - length prefix))
  else s

let _strip_suffix ~suffix s =
  if String.ends_with ~suffix s then String.(sub s 0 (length s - length suffix))
  else s

let parse_module content =
  let open PyreAst.Parser in
  with_context (fun context ->
      match Concrete.parse_module ~context content with
      | Result.Error { Error.message; line; column; _ } ->
          let message =
            Format.sprintf "Parsing error at line %d, column %d: %s" line column
              message
          in
          failwith message
      | Result.Ok ast -> ast)

let sexp_no_loc_to_s to_sexp t : string =
  let rec strip_locs (sexp : Sexplib0.Sexp.t) =
    let open Sexplib0__Sexp in
    match sexp with
    | Atom _ -> sexp
    (*     | List (Atom "location" :: l) -> List (List.map strip_locs l) *)
    | List l ->
        let l' =
          List.filter
            (function List (Atom "location" :: _) -> false | _ -> true)
            l
        in
        List (List.map strip_locs l')
  in
  let sexp = t |> to_sexp |> strip_locs in
  let pp = Sexplib0.Sexp.pp_hum_indent 2 in
  sf "%a" pp sexp

module Code = struct
  let comment comment =
    let comment =
      comment |> String.trim
      |> Base.replace_string ~all:true (rex "\n\\s+") ~by:"\n"
    in
    sf "(* %s *)" comment

  let log_info s = sf "Log.info %S;" s
end

module Emit = struct
  let format fmt =
    let out s = print_endline s in
    Format.ksprintf out fmt

  let empty_line () = format ""

  let debug fmt =
    let out s =
      Format.eprintf "%s@." (Color.(apply ~use_colors:true FG.gray) s)
    in
    Format.ksprintf out fmt

  let sexp to_sexp v =
    Format.eprintf "%s\n@."
      (Color.(apply ~use_colors:true FG.blue) @@ sexp_no_loc_to_s to_sexp v)

  let sexp_truncated to_sexp v =
    Format.eprintf "%s\n@."
      (Color.(apply ~use_colors:true FG.blue)
         (Truncate.lines
            ~trailer:(fun _ -> " ...")
            10
            (sexp_no_loc_to_s to_sexp v)))

  let comment comment = format "%s" (Code.comment comment)
end

let translate output_header input_file =
  let tests = ref [] in
  let register_test fun_name = tests := fun_name :: !tests in
  let input = read_file input_file in
  let modu = parse_module input in
  (*   print_sexp_no_loc PyreAst.Concrete.Module.sexp_of_t modu; *)
  let to_ocaml_identifier = String.lowercase in
  let todo = "assert false" in
  let translate_module_body () =
    modu.body
    |> List.iter @@ fun stmt ->
       let open PyreAst.Concrete in
       match stmt with
       | Statement.Expr
           { value = Expression.Constant { value = String s; _ }; _ } ->
           Emit.sexp Statement.sexp_of_t stmt;
           Emit.comment (String.trim s)
       | Statement.Assign { targets = [ Name { id; _ } ]; _ } ->
           Emit.sexp Statement.sexp_of_t stmt;
           Emit.format "let %s = assert false"
             (Identifier.to_string id |> to_ocaml_identifier)
       | PyreAst.Concrete.Statement.FunctionDef
           {
             location = _;
             name;
             args;
             body;
             decorator_list = _;
             returns = _;
             type_comment = _;
           } ->
           Emit.sexp_truncated Statement.sexp_of_t stmt;
           let fun_comment_opt =
             match body with
             | Statement.Expr
                 { value = Expression.Constant { value = String s; _ }; _ }
               :: _ ->
                 Some s
             | _ -> None
           in
           let fun_name = Identifier.to_string name |> to_ocaml_identifier in
           let fun_args =
             match args with
             | {
              posonlyargs = _;
              args;
              vararg = _;
              kwonlyargs = _;
              kw_defaults = _;
              kwarg = _;
              defaults = _;
             } ->
                 let pytype_to_type (id : Identifier.t) =
                   match Identifier.to_string id with
                   | "str" -> "string"
                   | "List" -> "list"
                   | s -> "_unknown_" ^ to_ocaml_identifier s
                 in
                 let annotation_to_type (annotation : Expression.t option) =
                   match annotation with
                   | None -> None
                   | Some
                       (Expression.Subscript
                         {
                           location = _;
                           value = Name { id; _ };
                           slice = Name { id = slice_id; _ };
                           ctx = _;
                         }) ->
                       Some (pytype_to_type slice_id ^ " " ^ pytype_to_type id)
                   | _ -> None
                 in
                 args
                 |> List.map
                    @@ fun Argument.
                             {
                               location = _;
                               identifier;
                               annotation;
                               type_comment = _;
                             } ->
                    ( "_" ^ to_ocaml_identifier (Identifier.to_string identifier),
                      annotation_to_type annotation )
           in
           let fun_body = todo in
           Option.iter Emit.comment fun_comment_opt;
           Emit.format "let %s %s = %s" fun_name
             (String.concat " "
                (List.map
                   (function
                     | arg_name, None -> arg_name
                     | arg_name, Some arg_type ->
                         sf "(%s : %s)" arg_name arg_type)
                   fun_args))
             fun_body
       | PyreAst.Concrete.Statement.ClassDef
           { location = _; name; bases = _; keywords = _; body; decorator_list }
         ->
           let is_incremental =
             List.exists
               (function
                 | Expression.Attribute
                     {
                       value =
                         Expression.Attribute
                           { value = Expression.Name { id; _ }; _ };
                       attr;
                       _;
                     } ->
                     Identifier.to_string id = "pytest"
                     && Identifier.to_string attr = "incremental"
                 | _ -> false)
               decorator_list
           in
           Emit.sexp_truncated Statement.sexp_of_t stmt;
           let translate_test_class () =
             let test_fun_name =
               Identifier.to_string name |> Naming.of_camlcase
               |> Naming.to_snakecase
             in
             let fun_comment_opt =
               match body with
               | Statement.Expr
                   { value = Expression.Constant { value = String s; _ }; _ }
                 :: _ ->
                   Some s
               | _ -> None
             in
             let test_cases =
               body
               |> List.filter_map @@ function
                  | PyreAst.Concrete.Statement.FunctionDef { name; body; _ } ->
                      let fun_comment_opt =
                        match body with
                        | Statement.Expr
                            {
                              value =
                                Expression.Constant { value = String s; _ };
                              _;
                            }
                          :: _ ->
                            Some s
                        | _ -> None
                      in
                      let fun_name =
                        Identifier.to_string name |> to_ocaml_identifier
                      in
                      Some (fun_name, fun_comment_opt)
                  | _ -> None
             in
             let tezt_protocol_register_test ~title ~tags body =
               sf
                 {|Protocol.register_test
               ~__FILE__
               ~title:"%s"
               ~tags:%s
             @@@@ fun _protocol -> %s|}
                 title
                 ("["
                 ^ (String.concat "; " @@ List.map (fun tag -> sf "%S" tag) tags)
                 ^ "]")
                 body
             in
             let test_fun_body =
               let body =
                 (* okay, then work for 2-3 hours, then ping me so we
                    can do another round of review. then we cna have a
                    small meeting. when today would it suit you? i'm
                    available all afternoon except 16h-16h30 *)
                 let case_lines =
                   test_cases
                   |> List.map @@ fun (fun_name, fun_comment_opt) ->
                      let lines =
                        (Option.map Code.comment fun_comment_opt
                        |> Option.to_list)
                        @ [
                            Code.log_info
                              Naming.(
                                fun_name |> of_snakecase
                                |> strip_prefix ~prefix:[ "test" ]
                                |> to_title);
                          ]
                      in
                      String.concat "\n" lines
                 in
                 String.concat "\n" case_lines ^ "\nunit"
               in
               let tags =
                 Identifier.to_string name |> Naming.of_camlcase
                 |> Naming.strip_prefix ~prefix:[ "test" ]
               in
               let title =
                 Identifier.to_string name |> Naming.of_camlcase
                 |> Naming.strip_prefix ~prefix:[ "test" ]
                 |> Naming.to_title
               in
               tezt_protocol_register_test ~title ~tags body
             in
             register_test test_fun_name;
             Emit.debug "%s is incremental: %b" test_fun_name is_incremental;
             Option.iter Emit.comment fun_comment_opt;
             Emit.format "let %s =" test_fun_name;
             Emit.format "  %s" test_fun_body
           in
           if String.starts_with ~prefix:"Test" (Identifier.to_string name) then
             translate_test_class ()
           else ()
       | _ ->
           Emit.sexp_truncated Statement.sexp_of_t stmt;
           ()
  in
  if output_header then (
    let license = Base.read_file "bin/license.ml.inc" in
    Emit.format "%s" license;
    Emit.empty_line ();
    let component = "TODO" in
    let ocaml_file =
      let base =
        Filename.basename input_file
        |> strip_prefix ~prefix:"test_"
        |> Filename.remove_extension
      in
      base ^ ".ml"
    in
    let subject = "TODO" in
    Emit.format
      {|(* Testing
   -------
   Component:    %s
   Invocation:   dune exec tezt/tests/main.exe -- --file %s
   Subject:      %s
*)|}
      component ocaml_file subject;
    Emit.empty_line ());
  translate_module_body ();
  match !tests with
  | [] -> Emit.debug "No tests detected"
  | tests ->
      Emit.empty_line ();
      Emit.format "let register ~protocols = ";
      Emit.format "%s"
        (String.concat " ;\n"
        @@ List.map
             (fun test_fun_name -> sf "  %s protocols" test_fun_name)
             tests)

(*      | PyreAst.Concrete.Statement.AsyncFunctionDef _ -> _ *)
(*      | PyreAst.Concrete.Statement.Return _ -> _ *)
(*      | PyreAst.Concrete.Statement.Delete _ -> _ *)
(*      | PyreAst.Concrete.Statement.Assign _ -> _ *)
(*      | PyreAst.Concrete.Statement.AugAssign _ -> _ *)
(*      | PyreAst.Concrete.Statement.AnnAssign _ -> _ *)
(*      | PyreAst.Concrete.Statement.For _ -> _ *)
(*      | PyreAst.Concrete.Statement.AsyncFor _ -> _ *)
(*      | PyreAst.Concrete.Statement.While _ -> _ *)
(*      | PyreAst.Concrete.Statement.If _ -> _ *)
(*      | PyreAst.Concrete.Statement.With _ -> _ *)
(*      | PyreAst.Concrete.Statement.AsyncWith _ -> _ *)
(*      | PyreAst.Concrete.Statement.Match _ -> _ *)
(*      | PyreAst.Concrete.Statement.Raise _ -> _ *)
(*      | PyreAst.Concrete.Statement.Try _ -> _ *)
(*      | PyreAst.Concrete.Statement.Assert _ -> _ *)
(*      | PyreAst.Concrete.Statement.Import _ -> _ *)
(*      | PyreAst.Concrete.Statement.ImportFrom _ -> _ *)
(*      | PyreAst.Concrete.Statement.Global _ -> _ *)
(*      | PyreAst.Concrete.Statement.Nonlocal _ -> _ *)
(*      | PyreAst.Concrete.Statement.Pass _ -> _ *)
(*      | PyreAst.Concrete.Statement.Break _ -> _ *)
(*      | PyreAst.Concrete.Statement.Continue _ -> _ *)

let () =
  let args =
    Simple_args.(Unit <*> Switch ("--header", "Output LICENSE and test header"))
  in
  let usage arg0 =
    Printf.sprintf "Usage: %s [options] [clic_man_output.xml ...]\nOptions are:"
      arg0
  in
  Simple_args.parse_arg_run ~usage args
  @@ fun ((), output_header) input_files ->
  List.iter (translate output_header) input_files
